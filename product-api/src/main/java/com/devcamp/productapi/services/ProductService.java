package com.devcamp.productapi.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.devcamp.productapi.models.Product;
import com.devcamp.productapi.respository.ProductRespository;

@Service
public class ProductService {
    private final ProductRespository productRespository;

    public ProductService(ProductRespository productRespository) {
        this.productRespository = productRespository;
    }

    public void createProduct() {
        List<Product> products = new ArrayList<>();

        products.add(new Product("Sản phẩm 1", "SP001", 100000, new Date(), null));
        products.add(new Product("Sản phẩm 2", "SP002", 150000, new Date(), null));
        products.add(new Product("Sản phẩm 3", "SP003", 200000, new Date(), null));

    }

    public List<Product> getProducts() {
        return (List<Product>) productRespository.findAll();
    }
}
