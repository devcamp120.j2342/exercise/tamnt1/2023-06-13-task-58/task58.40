package com.devcamp.productapi.respository;

import org.springframework.data.repository.CrudRepository;

import com.devcamp.productapi.models.Product;

public interface ProductRespository extends CrudRepository<Product, Integer> {

}
